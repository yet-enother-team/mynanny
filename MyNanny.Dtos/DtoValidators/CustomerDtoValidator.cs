﻿using System.Linq;
using FluentValidation;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Dtos.DtoValidators
{
    public class CustomerDtoValidator : AbstractValidator<CustomerDto>
    {
        public CustomerDtoValidator()
        {
            // First Name
            RuleFor(c => c.FirstName).NotNull().NotEmpty();

            RuleFor(c => c.FirstName).MaximumLength(30)
                .WithMessage("First name cannot be longer than 30 characters.");

            RuleFor(c => c.FirstName)
                .Must(c => !c.Any(char.IsDigit))
                .WithMessage("First name cannot contain any digits.");

            // Last Name
            RuleFor(c => c.LastName).NotNull().NotEmpty();

            RuleFor(c => c.LastName).MaximumLength(30)
                .WithMessage("Last name cannot be longer than 30 characters.");

            RuleFor(c => c.LastName)
                .Must(c => !c.Any(char.IsDigit))
                .WithMessage("Last name cannot contain any digits.");

            // Address
            RuleFor(c => c.Address).NotNull().NotEmpty().MaximumLength(60);

            // City
            RuleFor(c => c.City).NotNull().NotEmpty().MaximumLength(30);

            // Country
            RuleFor(c => c.Country).NotNull().NotEmpty().MaximumLength(30);

            // Phone
            RuleFor(c => c.Phone).NotNull().NotEmpty();

            RuleFor(c => c.Phone).MaximumLength(30).WithMessage("Phone number cannot be longer than 30 characters.");

            RuleFor(c => c.Phone).Matches(@"^[+][(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$")
                .WithMessage("Invalid phone number format.");
        }
    }
}