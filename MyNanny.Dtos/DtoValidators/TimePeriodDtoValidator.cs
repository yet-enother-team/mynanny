﻿using System;
using FluentValidation;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Dtos.DtoValidators
{
    public class TimePeriodDtoValidator : AbstractValidator<TimePeriodDto>
    {
        public TimePeriodDtoValidator()
        {
            // StartTime
            RuleFor(x => x.StartTime).NotNull().NotEmpty();

            RuleFor(x => x.StartTime)
                .Must(startTime => startTime > DateTime.UtcNow)
                .WithMessage("StartTime cannot be in the past.");

            // EndTime
            RuleFor(x => x.EndTime).NotNull().NotEmpty();

            RuleFor(x => x.EndTime)
                .Must((tp, endTime) => endTime > tp.StartTime)
                .WithMessage("EndTime cannot be earlier than StartTime");

            // TimePeriodTypeDto
            RuleFor(x => x.TimePeriodType).NotNull().NotEmpty().IsInEnum();
        }
    }
}