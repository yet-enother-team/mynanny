﻿using System;
using System.Linq;
using FluentValidation;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Dtos.DtoValidators
{
    public class NannyDtoValidator : AbstractValidator<NannyDto>
    {
        public NannyDtoValidator()
        {
            // First Name
            RuleFor(n => n.FirstName).NotNull().NotEmpty();

            RuleFor(n => n.FirstName).MaximumLength(30)
                .WithMessage("First name cannot be longer than 30 characters.");

            RuleFor(n => n.FirstName)
                .Must(n => !n.Any(char.IsDigit))
                .WithMessage("First name cannot contain any digits.");

            // Last Name
            RuleFor(n => n.LastName).NotNull().NotEmpty();

            RuleFor(n => n.LastName).MaximumLength(30)
                .WithMessage("Last name cannot be longer than 30 characters.");

            RuleFor(n => n.LastName)
                .Must(n => !n.Any(char.IsDigit))
                .WithMessage("Last name cannot contain any digits.");

            // Address
            RuleFor(n => n.Address).NotNull().NotEmpty().MaximumLength(30);

            // City
            RuleFor(n => n.City).NotNull().NotEmpty().MaximumLength(30);

            // Country
            RuleFor(n => n.Country).NotNull().NotEmpty().MaximumLength(30);

            // Phone
            RuleFor(n => n.Phone).NotNull().NotEmpty();

            RuleFor(n => n.Phone).MaximumLength(30).WithMessage("Phone number cannot be longer than 30 characters.");

            RuleFor(n => n.Phone).Matches(@"^[+][(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$")
                .WithMessage("Invalid phone number format.");

            // Date of birth
            RuleFor(n => n.DateOfBirth).NotNull().NotEmpty();
            RuleFor(n => n.DateOfBirth).Must(dob =>
            {
                var age = DateTime.Now.Year - dob.Year;
                if (DateTime.Now.DayOfYear < dob.DayOfYear)
                    age -= 1;
                return age >= 20;
            }).WithMessage("Must be at least 20 years old.");

            RuleFor(x => x.StudyAddress).MaximumLength(60);
            RuleFor(x => x.WorkAddress).MaximumLength(60);

            // Has Passed Medical Clearance
            RuleFor(n => n.HasPassedMedicalClearance)
                .NotNull()
                .WithMessage("You must indicate whether or not you have passed the medical clearance test.");
        }
    }
}