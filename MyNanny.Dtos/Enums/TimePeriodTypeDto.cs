﻿namespace MyNanny.Dtos.Enums
{
    public enum TimePeriodTypeDto : byte
    {
        Available = 1,
        NotAvailable = 2
    }
}