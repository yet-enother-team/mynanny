﻿namespace MyNanny.Dtos.Enums
{
    public enum OrderStatusDto
    {
        Requested = 1,
        Confirmed = 2,
        Denied = 3,
        Cancelled = 4
    }
}
