﻿using System;
using MyNanny.Dtos.Enums;

namespace MyNanny.Dtos.Dtos
{
    public class TimePeriodDto : BaseDto
    {
        public Guid NannyId { get; internal set; }

        public DateTimeOffset StartTime { get; set; }

        public DateTimeOffset EndTime { get; set; }

        public TimePeriodTypeDto TimePeriodType { get; set; }
    }
}