﻿using System;

namespace MyNanny.Dtos.Dtos
{
    public class BaseDto
    {
        public Guid Id { get; internal set; }
    }
}