﻿namespace MyNanny.Dtos.Dtos
{
    public class CustomerDto : BaseDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }
    }
}