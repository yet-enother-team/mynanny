﻿using System;

namespace MyNanny.Dtos.Dtos
{
    public class NannyDto : BaseDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string WorkAddress { get; set; }

        public string StudyAddress { get; set; }

        public bool HasPassedMedicalClearance { get; set; }
    }
}