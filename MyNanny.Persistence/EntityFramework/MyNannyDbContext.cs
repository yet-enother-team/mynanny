﻿using Microsoft.EntityFrameworkCore;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;

namespace MyNanny.Persistence.EntityFramework
{
    public class MyNannyDbContext : DbContext, IMyNannyDbContext
    {
        public MyNannyDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Nanny> Nannies { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<TimePeriod> TimePeriods { get; set; }
    }
}