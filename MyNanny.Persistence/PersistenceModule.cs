﻿using Autofac;
using MyNanny.Application.Interfaces;
using MyNanny.Persistence.EntityFramework;

namespace MyNanny.Persistence
{
    public class PersistenceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MyNannyDbContext>().As<IMyNannyDbContext>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
        }
    }
}