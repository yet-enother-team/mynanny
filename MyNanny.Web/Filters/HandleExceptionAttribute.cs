﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using MyNanny.Application.Exceptions;

namespace MyNanny.Web.Filters
{
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var error = GetCustomErrorResponse(context);

            if (context.Exception is NotFoundException)
            {
                context.Result = new ObjectResult(error)
                {
                    StatusCode = 404
                };

                context.ExceptionHandled = true;

            }

            else if (context.Exception is RangeIsOccupiedException)
            {
                context.Result = new ObjectResult(error)
                {
                    StatusCode = 409
                };

                context.ExceptionHandled = true;
            }
            
            else if (context.Exception is NotWorkingHoursException)

            {
                context.Result = new ObjectResult(error)
                {
                    StatusCode = 409
                };

                context.ExceptionHandled = true;
            }
        }


        public static CustomErrorResponse GetCustomErrorResponse(ExceptionContext context)
        {
            return new CustomErrorResponse(false, new [] { context.Exception.Message });
        }

        public class CustomErrorResponse
        {
            public bool Success;

            public string[] Errors;

            public CustomErrorResponse(bool success, string[] errors)
            {
                Success = success;
                Errors = errors;
            }
        }
    }
}
