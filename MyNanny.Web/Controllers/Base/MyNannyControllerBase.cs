﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNanny.Web.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyNanny.Web.Controllers.Base
{
    [ApiController]
    [ValidateModel]
    [HandleException]
    [Route("api/[controller]")]
    public abstract class MyNannyControllerBase : ControllerBase
    {
       
    }
}
