﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNanny.Application.Exceptions;
using MyNanny.Application.Interactors.Customers;
using MyNanny.Dtos.Dtos;
using MyNanny.Web.Controllers.Base;
using MyNanny.Web.Filters;

namespace MyNanny.Web.Controllers
{
    public class CustomersController : MyNannyControllerBase
    {
        private readonly CustomerService _manager;

        public CustomersController(CustomerService manager)
        {
            _manager = manager;
        }

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>An array of customers</returns>
        [HttpGet("")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _manager.GetAsync());
        }

        /// <summary>
        /// Gets the customer by id
        /// </summary>
        /// <param name="id">An Id of Customer object to retrieve</param>
        /// <returns>A customer object with a given id</returns>
        /// <response code="200">Returns the customer with a given id</response>
        /// <reponse code="404">A customer with a given id was not found</reponse>
        /// <response code="400">If id is incorrectly formatted or of incorrect type</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            return Ok(await _manager.GetAsync(id));
        }

        /// <summary>
        ///     Creates a new customer in the database
        /// </summary>
        /// <param name="customerDto">Customer object to create</param>
        /// <returns>A newly created customer object</returns>
        /// <response code="201">Returns the newly created customer object</response>
        /// <response code="400">If the item does not pass validation</response>
        [HttpPost("")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] CustomerDto customerDto)
        {
            var response = await _manager.CreateAsync(customerDto);
            return CreatedAtAction(nameof(Get), new {id = response.Id}, response);
        }


        /// <summary>
        ///     Updates a customer with a given id
        /// </summary>
        /// <param name="id">An id of Customer object to update</param>
        /// <param name="customerDto">Updated Customer object</param>
        /// <returns>Updated Customer object</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] CustomerDto customerDto)
        {
            return Ok(await _manager.UpdateAsync(id, customerDto));
        }

        /// <summary>
        ///     Deletes a Customer object with a given id
        /// </summary>
        /// <param name="id">An id of Customer object to delete</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var succeeded = await _manager.DeleteAsync(id);
            
            return succeeded ? Ok() : StatusCode(500);
        }
    }
}