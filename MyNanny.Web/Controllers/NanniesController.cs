﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNanny.Application.Exceptions;
using MyNanny.Application.Interactors.Nannies;
using MyNanny.Dtos.Dtos;
using MyNanny.Web.Controllers.Base;
using MyNanny.Web.Filters;

namespace MyNanny.Web.Controllers
{
    public partial class NanniesController : MyNannyControllerBase
    {
        private readonly NannyService _manager;

        public NanniesController(NannyService manager)
        {
            _manager = manager;
        }

        /// <summary>
        ///     Gets all the Nannies
        /// </summary>
        /// <returns>An array of Nannies</returns>
        [HttpGet("")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<NannyDto>>> Get()
        {
            return Ok(await _manager.GetAsync());
        }

        /// <summary>
        ///     Gets the Nanny by id
        /// </summary>
        /// <param name="id">An Id of Nanny object to retrieve</param>
        /// <returns>A Nanny object with a given id</returns>
        /// <response code="200">Returns the Nanny with a given id</response>
        /// <reponse code="404">A Nanny with a given id was not found</reponse>
        /// <response code="400">If id is incorrectly formatted or of incorrect type</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<NannyDto>> Get([FromRoute] Guid id)
        {
            return Ok(await _manager.GetAsync(id));
        }

        /// <summary>
        ///     Creates a new Nanny in the database
        /// </summary>
        /// <param name="nannyDto">Nanny object to create</param>
        /// <returns>A newly created Nanny object</returns>
        /// <response code="201">Returns the newly created Nanny object</response>
        /// <response code="400">If the item does not pass validation</response>
        [HttpPost("")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<NannyDto>> Post([FromBody] NannyDto nannyDto)
        {
            var response = await _manager.CreateAsync(nannyDto);
            return CreatedAtAction(nameof(Get), new {id = response.Id}, response);
        }


        /// <summary>
        ///     Updates a Nanny with a given id
        /// </summary>
        /// <param name="id">An id of Nanny object to update</param>
        /// <param name="nannyDto">Updated Nanny object</param>
        /// <returns>Updated Nanny object</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Put([FromRoute] Guid id, [FromBody] NannyDto nannyDto)
        {
            return Ok(await _manager.UpdateAsync(id, nannyDto));
        }

        /// <summary>
        ///     Deletes a Nanny object with a given id
        /// </summary>
        /// <param name="id">An id of Nanny object to delete</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Delete([FromRoute] Guid id)
        {
            var succeeded = await _manager.DeleteAsync(id);
            
            return succeeded ? Ok() : StatusCode(500);
        }
    }
}