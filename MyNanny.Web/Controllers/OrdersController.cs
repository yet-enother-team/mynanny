﻿using MyNanny.Application.Interactors.Orders;
using MyNanny.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNanny.Web.Controllers
{
    public class OrdersController : MyNannyControllerBase
    {
        private readonly OrderService _manager;
        
        public OrdersController(OrderService manager)
        {
            _manager = manager;
        }

        // Get orders

        // Create Orders

        // Update Orders

        // Cancel Orders

        // Delete Orders
    }
}
