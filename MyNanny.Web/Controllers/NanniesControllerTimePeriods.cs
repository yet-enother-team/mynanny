﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNanny.Application.Exceptions;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Web.Controllers
{
    public partial class NanniesController
    {
        [HttpGet("{id}/TimePeriods")]
        public async Task<ActionResult<List<TimePeriodDto>>> GetTimePeriods([FromRoute] Guid id)
        { 
            return Ok(await _manager.GetTimePeriodsAsync(id));
        }

        [HttpPost("{id}/TimePeriods")]
        public async Task<ActionResult<TimePeriodDto>> PostTimePeriod([FromRoute] Guid id,
            [FromBody] TimePeriodDto timePeriod)
        {
            return Ok(await _manager.CreateTimePeriodAsync(id, timePeriod));
        }
    }
}