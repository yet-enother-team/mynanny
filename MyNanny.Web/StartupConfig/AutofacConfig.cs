﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using MyNanny.Application;
using MyNanny.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNanny.Web.StartupConfig
{
    public static class AutofacConfig
    {
        public static IServiceProvider GetAutofacServiceProvider(this IServiceCollection services)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<ApplicationModule>();
            builder.RegisterModule<PersistenceModule>();
            builder.RegisterModule<WebModule>();

            builder.Populate(services);
            var container = builder.Build();

            return new AutofacServiceProvider(container);
        }
    }
}
