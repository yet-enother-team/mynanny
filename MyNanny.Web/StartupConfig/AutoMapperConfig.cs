﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MyNanny.Application.Mapping;

namespace MyNanny.Web.StartupConfig
{
    public static class AutoMapperConfig
    {
        public static IServiceCollection AddAutoMapperMeta(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<CustomerMappingProfile>();
                mc.AddProfile<NannyMappingProfile>();
                mc.AddProfile<OrderMappingProfile>();
                mc.AddProfile<TimePeriodMappingProfile>();
                mc.AddProfile<TimePeriodTypeMappingProfile>();
            });

            IMapper mapper = mappingConfig.CreateMapper();
            return services.AddSingleton(mapper);
        }
    }
}
