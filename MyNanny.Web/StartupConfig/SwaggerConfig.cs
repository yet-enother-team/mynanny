﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace MyNanny.Web.StartupConfig
{
    public static class SwaggerConfig
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services, string apiTitle)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info 
                { 
                    Title = apiTitle, 
                    Version = "v1" 
                });
            });
            return services;
        }
        
        public static IApplicationBuilder UseSwaggerMeta(this IApplicationBuilder app, string apiTitle)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", apiTitle);
                c.RoutePrefix = string.Empty;
            });
            return app;
        }
    }
}
