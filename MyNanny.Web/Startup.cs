﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyNanny.Application;
using MyNanny.Application.Interfaces;
using MyNanny.Dtos.Dtos;
using MyNanny.Persistence;
using MyNanny.Persistence.EntityFramework;
using MyNanny.Web.StartupConfig;
using Swashbuckle.AspNetCore.Swagger;

namespace MyNanny.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ApiTitle = Configuration["ApiTitle"];
        }

        public IConfiguration Configuration { get; }

        public string ApiTitle { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<BaseDto>());

            // EF Core
            var connectionString = Configuration["Persistence:LocalDbMSSQL"];
            services.AddDbContext<IMyNannyDbContext, MyNannyDbContext>(
                options => options.UseSqlServer(connectionString, b => b.MigrationsAssembly("MyNanny.Persistence")));

            // Add Swagger
            services.AddSwagger(ApiTitle);

            services.AddAutoMapperMeta();

            // Add Autofac
            return services.GetAutofacServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            // Swagger
            app.UseSwaggerMeta(ApiTitle);

            app.UseMvc();
        }
    }
}