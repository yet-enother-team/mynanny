﻿namespace MyNanny.Domain.Enums
{
    public enum OrderStatus : byte
    {
        Requested = 1,
        Confirmed = 2,
        Denied = 3,
        Cancelled = 4
    }
}
