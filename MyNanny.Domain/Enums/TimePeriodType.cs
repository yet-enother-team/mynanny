﻿namespace MyNanny.Domain.Enums
{
    public enum TimePeriodType : byte
    {
        Available = 1,
        NotAvailable = 2
    }
}