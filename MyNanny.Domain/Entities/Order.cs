﻿using System;
using System.ComponentModel.DataAnnotations;
using MyNanny.Domain.Entities.Base;
using MyNanny.Domain.Enums;

namespace MyNanny.Domain.Entities
{
    public class Order : BaseEntity
    {
        public Customer Customer { get; set; }
        [Required]
        public Guid CustomerId { get; set; }

        public Nanny Nanny { get; set; }
        [Required]
        public Guid NannyId { get; set; }

        [Required]
        public DateTimeOffset StartTime { get; set; }

        [Required]
        public DateTimeOffset EndTime { get; set; }

        [Required]
        [MaxLength(60)]
        public string Address { get; set; }

        public OrderStatus OrderStatus { get; internal set; }
    }
}