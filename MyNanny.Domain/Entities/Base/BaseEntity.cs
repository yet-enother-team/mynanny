﻿using System;

namespace MyNanny.Domain.Entities.Base
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}