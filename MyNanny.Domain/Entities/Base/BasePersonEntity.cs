﻿using System.ComponentModel.DataAnnotations;

namespace MyNanny.Domain.Entities.Base
{
    public class BasePersonEntity : BaseEntity
    {
        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(60)]
        public string Address { get; set; }

        [Required]
        [MaxLength(30)]
        public string City { get; set; }

        [Required]
        [MaxLength(30)]
        public string Country { get; set; }

        [Required]
        [Phone]
        [MaxLength(30)]
        public string Phone { get; set; }
    }
}   