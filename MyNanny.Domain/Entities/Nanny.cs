﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyNanny.Domain.Entities.Base;

namespace MyNanny.Domain.Entities
{
    public class Nanny : BasePersonEntity
    {
        [Required]
        public DateTime DateOfBirth { get; set; }

        [MaxLength(60)]
        public string WorkAddress { get; set; }

        [MaxLength(60)]
        public string StudyAddress { get; set; }

        [Required]
        public bool HasPassedMedicalClearance { get; set; }

        public ICollection<TimePeriod> TimePeriods { get; set; }
    }
}