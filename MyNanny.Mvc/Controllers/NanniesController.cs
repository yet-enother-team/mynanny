﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyNanny.Mvc.Data;
using MyNanny.Mvc.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyNanny.Mvc.Controllers
{
    public class NanniesController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _db;

        public NanniesController(UserManager<IdentityUser> userManager, 
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        [Authorize]
        public IActionResult Index()
        {
            var nannies = _userManager.GetUsersInRoleAsync("Nanny").Result;
            ViewData["Nannies"] = nannies;
            return View();
        }

        [Authorize(Roles = "Customer")]
        public IActionResult OrderNanny(IdentityUser nanny, DateTimeOffset orderForTime)
        {
            var order = new Order()
            {
                CreatedBy = _userManager.GetUserAsync(User).Result,
                RequestedNanny = nanny,
                TimeAndDate = orderForTime
            };

            _db.Orders.Add(order);

            return RedirectToAction("Index");
        }
    }
}
