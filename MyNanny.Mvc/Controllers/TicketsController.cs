﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using MyNanny.Mvc.Data;
using MyNanny.Mvc.Models;

namespace MyNanny.Mvc.Controllers
{
    public class TicketsController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IdentityUser _user;
        private readonly ApplicationDbContext _db;

        public TicketsController(UserManager<IdentityUser> userManager, 
            IdentityUser user,
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _user = user;
            _db = db;
        }

        [Authorize(Roles = "Customer, Helpdesk")]
        public IActionResult Index()
        {
            List<Ticket> tickets = null;
            if (_userManager.IsInRoleAsync(_user, "Customer").Result)
            {
                tickets = _db.Tickets.Where(x => x.CreatedBy == _user).ToList();
            }
            else if (_userManager.IsInRoleAsync(_user, "Helpdesk").Result)
            {
                tickets = _db.Tickets.ToList();
            }
            
            ViewData["tickets"] = tickets;
            return View();
        }

        [Authorize(Roles = "Customer")]
        public IActionResult Create()
        {
            return View(new Ticket());
        }
 
        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> Create(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                await _db.Tickets.AddAsync(ticket);
            }
            return Index();
        }

        public IActionResult ViewTicketInfo(Ticket ticket)
        {
            return View(ticket);
        }
    }
}