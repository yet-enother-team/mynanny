﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNanny.Mvc.Models
{
    public class Order
    {
        public Guid Id { get; set; }

        public IdentityUser CreatedBy { get; set; }

        public IdentityUser RequestedNanny { get; set; }

        public DateTimeOffset TimeAndDate { get; set; }
    }
}
