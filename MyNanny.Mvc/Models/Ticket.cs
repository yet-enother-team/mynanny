﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MyNanny.Mvc.Models
{
    public class Ticket
    {
        public Guid Id { get; set; }

        public IdentityUser CreatedBy { get; set; }

        public string Content { get; set; }

        public TicketStatus Status { get; set; }
    }
}
