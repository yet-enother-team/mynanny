﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MyNanny.Application.Exceptions;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities.Base;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Interactors
{
    public abstract class AbstractService<TDto, TEntity> : IManagerService<TDto, TEntity>
        where TDto : BaseDto where TEntity : BaseEntity
    {
        protected readonly IRepository<TEntity> Repository;
        protected readonly IMapper Mapper;

        protected AbstractService(IRepository<TEntity> repository, IMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }

        public virtual async Task<List<TDto>> GetAsync()
        {
            var entities = await Repository.ListAsync();
            var entityDtos = new List<TDto>(entities.Count());
            Mapper.Map(entities, entityDtos);
            return entityDtos;
        }

        public async Task<TDto> GetAsync(Guid id)
        {
            var entity = await Repository.GetByIdAsync(id);

            if (entity == null)
            {
                throw new NotFoundException(typeof(TEntity).Name, id);
            }
            
            return Mapper.Map<TDto>(entity);
        }

        public virtual async Task<TDto> CreateAsync(TDto entityDto)
        {
            var entity = Mapper.Map<TDto, TEntity>(entityDto);
            await Repository.AddAsync(entity);
            entityDto.Id = entity.Id;
            return entityDto;
        }

        public virtual async Task<TDto> UpdateAsync(Guid id, TDto entityDto)
        {
            var entityToUpdate = await Repository.GetByIdAsync(id);

            if (entityToUpdate == null)
            {
                throw new NotFoundException(typeof(TEntity).Name, id);
            }
            
            Mapper.Map(entityDto, entityToUpdate);
            await Repository.EditAsync(entityToUpdate);
            entityDto.Id = id;
            return entityDto;
        }

        public virtual async Task<bool> DeleteAsync(Guid id)
        {
            var entityToDelete = await Repository.GetByIdAsync(id);

            if (entityToDelete == null)
            {
                throw new NotFoundException(typeof(TEntity).Name, id);
            }
            
            await Repository.DeleteAsync(entityToDelete);
            return true;
        }
    }
}