﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MyNanny.Application.Exceptions;
using MyNanny.Application.Interactors.TimePeriods;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;
using TimePeriodType = MyNanny.Domain.Enums.TimePeriodType;

namespace MyNanny.Application.Interactors.Nannies
{
    public class NannyService : AbstractService<NannyDto, Nanny>
    {
        protected internal readonly TimePeriodService TimePeriodsManager;

        public NannyService(IRepository<Nanny> repository,
            TimePeriodService timePeriodsManager, IMapper mapper)
            : base(repository, mapper)
        {
            TimePeriodsManager = timePeriodsManager;
        }

        public async Task<List<TimePeriodDto>> GetTimePeriodsAsync(Guid nannyId)
        {
            var nanny = await Repository.GetByIdAsync(nannyId);
            if (nanny == null)
            {
                throw new NotFoundException(nameof(Nanny), nannyId);
            }
 

            return await TimePeriodsManager.GetByNannyIdAsync(nannyId);
        }

        public async Task<TimePeriodDto> CreateTimePeriodAsync(Guid nannyId, TimePeriodDto timePeriodDto)
        {
            var nanny = await Repository.GetByIdAsync(nannyId);
            if (nanny == null)
            {
                throw new NotFoundException(nameof(Nanny), nannyId);
            }

            return await TimePeriodsManager.CreateTimePeriodAsync(nannyId, timePeriodDto);
        }
    }
}