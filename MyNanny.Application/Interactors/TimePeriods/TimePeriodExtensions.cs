﻿using System;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Interactors.TimePeriods
{
    public static class TimePeriodExtensions
    {
        public static bool HasIntersection(this TimePeriodDto firstTimePeriodDto, 
            DateTimeOffset secondTimePeriodStart, DateTimeOffset secondTimePeriodEnd)
        {
            var start = firstTimePeriodDto.StartTime;
            var end = firstTimePeriodDto.EndTime;

            if (start > secondTimePeriodEnd)
                return false;

            if (end < secondTimePeriodStart)
                return false;

            return true;
        }
    }
}