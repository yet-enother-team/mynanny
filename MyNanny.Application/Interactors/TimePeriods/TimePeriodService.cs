﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MyNanny.Application.Exceptions;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;
using MyNanny.Domain.Enums;
using MyNanny.Dtos.Dtos;
using MyNanny.Dtos.Enums;

namespace MyNanny.Application.Interactors.TimePeriods
{
    public class TimePeriodService : AbstractService<TimePeriodDto, TimePeriod>
    {
        public TimePeriodService(IRepository<TimePeriod> repository, IMapper mapper)
            : base(repository, mapper)
        {
        }

        // Get a list of Time Periods by Nanny ID
        public async Task<List<TimePeriodDto>> GetByNannyIdAsync(Guid nannyId)
        {
            var timePeriods = (await Repository.ListAsync(t => t.NannyId == nannyId)).ToList();

            if (timePeriods.Any())
            {
                var timePeriodDtos = new List<TimePeriodDto>(timePeriods.Count());

                foreach (var timePeriod in timePeriods)
                {
                    var timePeriodDto = Mapper.Map<TimePeriodDto>(timePeriod);
                    timePeriodDtos.Add(timePeriodDto);
                }

                return timePeriodDtos;
            }

            return new List<TimePeriodDto>();
        }

        // Creates a TimePeriod with Booked type
        public async Task<TimePeriodDto> CreateTimePeriodAsync(Guid nannyId, TimePeriodDto timePeriodDto)
        {
            timePeriodDto.NannyId = nannyId;

            if (timePeriodDto.TimePeriodType == TimePeriodTypeDto.Available)
            {
                return await CreateAvailableAsync(nannyId, timePeriodDto);
            }

            if (await RangeIsOccupiedAsync(nannyId, timePeriodDto))
            {
                throw new RangeIsOccupiedException(nameof(Nanny), nannyId);
            }

            // Get the surrounding Available TimePeriod
            var surroundingTimePeriod = await GetSurroundingAvailableTimePeriod(nannyId, timePeriodDto);

            if (surroundingTimePeriod == null)
            {
                throw new NotWorkingHoursException(nameof(Nanny), nannyId, timePeriodDto.StartTime, timePeriodDto.EndTime);
            }

            // Split the Available TimePeriod and put the new TimePeriod within
            var timePeriod = Mapper.Map<TimePeriod>(timePeriodDto);

            var rightStart = timePeriod.EndTime;
            var rightEnd = surroundingTimePeriod.EndTime;
            var newTimePeriodToTheRight = new TimePeriod
            {
                NannyId = nannyId,
                StartTime = rightStart,
                EndTime = rightEnd,
                TimePeriodType = TimePeriodType.Available
            };

            surroundingTimePeriod.EndTime = timePeriod.StartTime;

            await Repository.AddAsync(newTimePeriodToTheRight);
            await Repository.AddAsync(timePeriod);

            timePeriodDto.Id = timePeriod.Id;
            return timePeriodDto;
        }

        // Creates a TimePeriod with Available type
        private async Task<TimePeriodDto> CreateAvailableAsync(Guid nannyId, TimePeriodDto timePeriodDto)
        {
            timePeriodDto.NannyId = nannyId;

            if (await RangeIsOccupiedAsync(nannyId, timePeriodDto))
            {
                throw new RangeIsOccupiedException(nameof(Nanny), nannyId);
            }


            var intersections =
                await GetIntersectionsOfTypeAsync(nannyId, timePeriodDto, TimePeriodTypeDto.Available);

            if (intersections.Any())
            {
                var timePeriod = Mapper.Map<TimePeriod>(timePeriodDto);
                intersections.Add(timePeriod);
                var mergedTimePeriod = MergeIntersectingAvailables(intersections);
                intersections.Remove(timePeriod);
                
                await Repository.AddAsync(mergedTimePeriod);
                await Repository.DeleteAsync(intersections.ToArray());

                timePeriodDto = Mapper.Map<TimePeriodDto>(mergedTimePeriod);
                timePeriodDto.Id = mergedTimePeriod.Id;
            }
            else
            {
                var timePeriod = Mapper.Map<TimePeriod>(timePeriodDto);
                await Repository.AddAsync(timePeriod);
                timePeriodDto.Id = timePeriod.Id;
            }

            return timePeriodDto;
        }

        // Check if the Nanny already has a non-available time period with a given datetime range.
        private async Task<bool> RangeIsOccupiedAsync(Guid nannyId, TimePeriodDto timePeriodDto)
        {
            var start = timePeriodDto.StartTime;
            var end = timePeriodDto.EndTime;

            // Non-empty list designates the range is occupied or intersects
            var intersections = await Repository
                .ListAsync(t => t.NannyId == nannyId
                                && t.TimePeriodType != TimePeriodType.Available
                                && timePeriodDto.HasIntersection(t.StartTime, t.EndTime));

            return intersections.Any();
        }

        // Get intersections of indicated TimePeriodTypes
        private async Task<List<TimePeriod>> GetIntersectionsOfTypeAsync(
            Guid nannyId, TimePeriodDto timePeriodDto, TimePeriodTypeDto timePeriodTypeDto)
        {
            var type = Mapper.Map<TimePeriodTypeDto, TimePeriodType>(timePeriodTypeDto);

            var intersections = (await Repository
                .ListAsync(t => t.NannyId == nannyId
                                && t.TimePeriodType == type
                                && timePeriodDto.HasIntersection(t.StartTime, t.EndTime)));

            return intersections;
        }

        // Merge Available time periods into one
        private static TimePeriod MergeIntersectingAvailables(IEnumerable<TimePeriod> timePeriods)
        {
            var periodDtos = timePeriods.ToList();
            var newStartTime = periodDtos.Min(t => t.StartTime);
            var newEndTime = periodDtos.Max(t => t.EndTime);

            return new TimePeriod
            {
                NannyId = periodDtos.First().NannyId,
                StartTime = newStartTime,
                EndTime = newEndTime,
                TimePeriodType = TimePeriodType.Available
            };
        }

        // Check if the TimePeriod is within any Available TimePeriod
        private async Task<TimePeriod> GetSurroundingAvailableTimePeriod(Guid nannyId, TimePeriodDto timePeriodDto)
        {
            var availables = await Repository.ListAsync(
                t => t.NannyId == nannyId &&
                     t.TimePeriodType == TimePeriodType.Available &&
                     t.StartTime < timePeriodDto.StartTime &&
                     t.EndTime > timePeriodDto.EndTime);

            return availables.SingleOrDefault();
        }
    }
}