﻿using AutoMapper;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Interactors.Customers
{
    public class CustomerService : AbstractService<CustomerDto, Customer>
    {
        public CustomerService(IRepository<Customer> repository, IMapper mapper) :
            base(repository, mapper)
        {
        }
    }
}