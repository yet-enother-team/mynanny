﻿using AutoMapper;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyNanny.Application.Interactors.Orders
{
    public class OrderService : AbstractService<OrderDto, Order>
    {
        public OrderService(IRepository<Order> repository, IMapper mapper)
            : base(repository, mapper)
        {

        }
    }
}
