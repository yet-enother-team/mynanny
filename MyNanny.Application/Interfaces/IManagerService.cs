﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNanny.Application.Interfaces
{
    public interface IManagerService<TDto, TEntity>
    {
        Task<List<TDto>> GetAsync();

        Task<TDto> GetAsync(Guid id);

        Task<TDto> CreateAsync(TDto entityDto);

        Task<TDto> UpdateAsync(Guid id, TDto entityDto);

        Task<bool> DeleteAsync(Guid id);
    }
}