﻿using AutoMapper;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Mapping
{
    public class TimePeriodMappingProfile : Profile
    {
        public TimePeriodMappingProfile()
        {
            CreateMap<TimePeriodDto, TimePeriod>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}