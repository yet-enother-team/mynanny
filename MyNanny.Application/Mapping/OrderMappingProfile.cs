﻿using AutoMapper;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Mapping
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderDto, Order>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
