﻿using AutoMapper;
using MyNanny.Domain.Enums;
using MyNanny.Dtos.Enums;

namespace MyNanny.Application.Mapping
{
    public class TimePeriodTypeMappingProfile : Profile
    {
        public TimePeriodTypeMappingProfile()
        {
            CreateMap<TimePeriodTypeDto, TimePeriodType>().ReverseMap();
        }
    }
}