﻿using AutoMapper;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Mapping
{
    public class NannyMappingProfile : Profile
    {
        public NannyMappingProfile()
        {
            CreateMap<NannyDto, Nanny>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}