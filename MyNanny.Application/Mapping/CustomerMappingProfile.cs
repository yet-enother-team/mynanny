﻿using AutoMapper;
using MyNanny.Domain.Entities;
using MyNanny.Dtos.Dtos;

namespace MyNanny.Application.Mapping
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<CustomerDto, Customer>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}