﻿using Autofac;
using AutoMapper;
using MyNanny.Application.Interactors.Customers;
using MyNanny.Application.Interactors.Nannies;
using MyNanny.Application.Interactors.Orders;
using MyNanny.Application.Interactors.TimePeriods;

namespace MyNanny.Application
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Managers
            builder.RegisterType<CustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<NannyService>().InstancePerLifetimeScope();
            builder.RegisterType<TimePeriodService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderService>().InstancePerLifetimeScope();
            
            builder.RegisterType<Mapper>().As<IMapper>().SingleInstance();
        }
    }
}