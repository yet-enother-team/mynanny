﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyNanny.Application.Exceptions
{
    public class NotWorkingHoursException : Exception
    {
        public NotWorkingHoursException(string name, object key, DateTimeOffset start, DateTimeOffset end)
            : base($"Entity {name} ({key}) does not have" +
                   $" working hours to completely satisfy the time period between {start} and {end}.")
        {
        }
    }
}
