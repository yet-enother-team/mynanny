﻿using System;

namespace MyNanny.Application.Exceptions
{
    public class RangeIsOccupiedException : Exception
    {
        public RangeIsOccupiedException(string name, object key)
            : base($"TimePeriod range for entity {name} ({key}) is occupied.")
        {
        }
    }
}