# Welcome to MyNanny project!

**MyNanny** is an application that lets individual users to sign up and order nannies. The application is composed of ASP.NET Core 2.2 project at the back-end and Angular application at the front-end.

## Brief functionality
There are 3 roles in the application:
 - **Customer**
 - **Nanny**
 - **Administrator**

**Customer** functionality:
 - Can sign up on his/her behalf
 - Fills profile and preferences (including address and contact details)
 - Can view currently available nannies, all nannies, or specify a date and time to filter out nannies that are available at that time
 - Can order a nanny by choosing an available nanny, providing required details (address, time, hours) and payment information
 - Can cancel the order unless the order is within 2 hours
 - Can prolong the order if there's a need for nanny's services after time has expired
 - Can calculate the cost of the service without ordering by providing hours and number of children
 - Can contact the administrator for support

**Nanny** functionality:
 - Can sign up only when provided a token (after passing an interview, security screening, medical checks, etc)
 - Fills profile, address, contact details, and is obliged to upload a photo
 - Fills her timetable with time slots when she's available. Time slot is occupied when nanny is ordered for that slot
 - When order is processed by the system, nanny receives a notification with the order details
 - Can contact the administrator for support
 
**Administrator** functionality:
 - Can view all the orders, sort and filter them (both the current orders and past orders)
 - Can view income daily, weekly, and monthly income reports
 - Receives support tickets from both customers and nannies

## Running locally

Required software:
 - ASP.NET Core 2.2
 - Visual Studio 2017+

Required packages are located in .csproj file.
Prior to running, please update "LocalDatabaseString" in **appsettings.Development.json** to connection string of your database and apply migrations.