using MyNanny.Application.Interactors.Customers;
using System;
using Xunit;
using Moq;
using MyNanny.Application.Interfaces;
using MyNanny.Domain.Entities;
using AutoMapper;
using MyNanny.Dtos.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNanny.UnitTests
{
    public class CustomerServiceTests
    {
        private IMapper mapper;
        private Mock<IRepository<Customer>> mockCustomerRepository;

        private void InitialSetup()
        {
            // Configure AutoMapper
            var mapperConfig = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Customer, CustomerDto>().ReverseMap();
            });
            mapper = mapperConfig.CreateMapper();

            // Create mock customer repository
            mockCustomerRepository = new Mock<IRepository<Customer>>();
        }

        [Fact]
        public async Task GetAsync_ReturnsListOfCustomerDtos()
        {
            // Create a Customer list
            var resultTask = Task<List<Customer>>.Factory.StartNew(() => GetSampleCustomerList());            
            
            // Setup CustomerService
            InitialSetup();
            mockCustomerRepository.Setup(x => x.ListAsync()).Returns(resultTask);
            var service = new CustomerService(mockCustomerRepository.Object, mapper);


            var customerDtoList = await service.GetAsync();
            var customerList = resultTask.Result;

            Assert.Equal(customerDtoList.Count, customerList.Count);
        }

        [Fact]
        public async Task GetAsync_ReturnsCustomerDtoById()
        {

        }
        private List<Customer> GetSampleCustomerList()
        {
            var firstCustomer = new Customer()
            {
                Id = Guid.Parse("989A605E-ED11-4097-A747-8BA105F222EE"),
                FirstName = "Aidil",
                LastName = "Umarov",
                Address = "Prospekt Manasa",
                City = "Jalal-Abad",
                Country = "Kyrgyzstan",
                Phone = "+15714024345"
            };

            var secondCustomer = new Customer()
            {
                Id = Guid.Parse("B5AA261E-8365-468C-87E5-80FECEBF298D"),
                FirstName = "Eliza",
                LastName = "Djeenkulova",
                Address = "Ulitsa Panfilova",
                City = "Bishkek",
                Country = "Kyrgyzstan",
                Phone = "+996550738775"
            };

            return new List<Customer>()
            {
                firstCustomer, 
                secondCustomer
            };
        }

        private List<CustomerDto> GetSampleCustomerDtoList()
        {
            var firstCustomerDto = new CustomerDto()
            {
                FirstName = "Aidil",
                LastName = "Umarov",
                Address = "Prospekt Manasa",
                City = "Jalal-Abad",
                Country = "Kyrgyzstan",
                Phone = "+15714024345"
            };

            var secondCustomerDto = new CustomerDto()
            {
                FirstName = "Eliza",
                LastName = "Djeenkulova",
                Address = "Ulitsa Panfilova",
                City = "Bishkek",
                Country = "Kyrgyzstan",
                Phone = "+996550738775"
            };

            return new List<CustomerDto>()
            {
                firstCustomerDto,
                secondCustomerDto
            };
        }
    }
}
